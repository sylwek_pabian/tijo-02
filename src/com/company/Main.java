package com.company;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Sylwester Pabian
 */
interface CsvReport {
    /** funkcja zwraca liste stringow - lista linii z pliku CSV */
    List<String> csvToListOfString();
}

/**
 * Interface LineConverter
 */
interface LineConverter {
    /** wykonuje konwersje linii zapisanych w odpowiednim
     formacie do listy obiektow typu Team
     funkcja przyjmuje liste linii z pliku CSV oraz separator rekordow */
    List<Team> toTeam(List<String> lines, String splitBy);
}

/**
 *  Represents a team in league table
 */
class Team {
    // prosze zaprojektowac klase Team
    // prosze zadbac o odpowiednia implementacje pol klasy
    // prosze zaimplementowac odpowiedni konstruktor oraz metody,
    // ktore zwracaja pola klasy
    // prosze zaimplementowac metode toString
    // przed implementacja prosze przeanalizowac plik CSV
    // (http://tomaszgadek.com/static/ekstraklasa.csv)
    // oraz klase Main

    private int id;
    private String name;
    private int games;
    private int points;
    private int wins;
    private int draws;
    private int loses;
    private String goals;

    /**
     *
     * @param id Place in league table
     * @param name Team name
     * @param games Number of games
     * @param points The number of points scored by the team
     * @param wins Number of wins
     * @param draws Number of draws
     * @param loses Number of loses
     * @param goals Ratio of the number of scored and lost goals
     */
    public Team(int id, String name, int games, int points, int wins, int draws, int loses, String goals) {
        this.id = id;
        this.name = name;
        this.games = games;
        this.points = points;
        this.wins = wins;
        this.draws = draws;
        this.loses = loses;
        this.goals = goals;
    }

    /**
     * Get the place of this Team
     * @return this Team place in league table
     */
    public int getId() {
        return id;
    }

    /**
     * Change the place of this Team
     * @param id Place in league table
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Get the name of this Team
     * @return this Team name
     */
    public String getName() {
        return name;
    }

    /**
     * Change the name of this Team
     * @param name name of this Team
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the games of this Team
     * @return this Team games
     */
    public int getGames() {
        return games;
    }

    /**
     * Change games of this Team
     * @param games number of games of this team
     */
    public void setGames(int games) {
        this.games = games;
    }

    /**
     * Get points of this Team
     * @return this Team points
     */
    public int getPoints() {
        return points;
    }

    /**
     * Change points of this Team
     * @param points number of points of this Team
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * Get wins of this Team
     * @return this Team wins
     */
    public int getWins() {
        return wins;
    }

    /**
     * Change wins of this Team
     * @param wins number of wins of this Team
     */
    public void setWins(int wins) {
        this.wins = wins;
    }

    /**
     * Get draws of this Team
     * @return this Team draws
     */
    public int getDraws() {
        return draws;
    }

    /**
     * Change draws of this Team
     * @param draws number of draws of this Team
     */
    public void setDraws(int draws) {
        this.draws = draws;
    }

    /**
     * Get loses of this Team
     * @return this Team loses
     */
    public int getLoses() {
        return loses;
    }

    /**
     * Change loses of this Team
     * @param loses number of loses of this Team
     */
    public void setLoses(int loses) {
        this.loses = loses;
    }

    /**
     * Get goals ratio of this Team
     * @return this Team goals ratio
     */
    public String getGoals() {
        return goals;
    }

    /**
     * Change goals ratio of this Team
     * @param goals goals ratio of this Team
     */
    public void setGoals(String goals) {
        this.goals = goals;
    }

    /**
     *
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", games=" + games +
                ", points=" + points +
                ", wins=" + wins +
                ", draws=" + draws +
                ", loses=" + loses +
                ", goals='" + goals + '\'' +
                '}';
    }
}

/**
 * A class implementing the LineConverter interface
 */
class LineConverterImpl implements LineConverter {
    // klasa LineConverterImpl powinna implementowac interfejs LineConverter
    // prosze zaimplementowac metode toTeam
    // przed implementacja prosze przeanalizowac klase Main
    /**
     * LineConverterImpl class should implement the Line Converter interface
     * @param lines
     * @param splitBy this is the sign of the separator
     * @return a list of Team objects
     */
    @Override
    public List<Team> toTeam(List<String> lines, String splitBy) {
        List<Team> teams = new ArrayList<>();
        for(String line: lines) {
            String[] foo = line.split(splitBy);
            Team team = new Team(Integer.parseInt(foo[0].trim()),
                    foo[1],
                    Integer.parseInt(foo[2].trim()),
                    Integer.parseInt(foo[3].trim()),
                    Integer.parseInt(foo[4].trim()),
                    Integer.parseInt(foo[5].trim()),
                    Integer.parseInt(foo[6].trim()),
                    foo[7]);
            teams.add(team);
        }
        return teams;
    }
}

/**
 * CsvReader is the class used to read data from a csv file
 */
class CsvReader implements CsvReport {
    // klasa CsvReader powinna implementowac interfejs CsvReport
    // prosze utworzyc odpowiedni konstruktor
    // oraz wlasna implementacje metody csvToListOfString
    // przed implementacja prosze przeanalizowac klase Main
    private String csv_file;

    /**
     * Constructor for class CsvReader
     * @param csv_file is a path to the csv file
     */
    public CsvReader(String csv_file) {
        this.csv_file = csv_file;
    }

    /**
     * Making list from csv files
     * @return this list of league table
     *         Read from ekstraklasa.csv file
     */
    @Override
    public List<String> csvToListOfString() {

        BufferedReader br = null;
        String line = "";
        List<String> leagueTable = new ArrayList<>();

        try {
            br = new BufferedReader(new FileReader(csv_file));
            br.readLine();
            while ((line = br.readLine()) != null) {
                leagueTable.add(line);
            }
            return leagueTable;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return leagueTable;
    }
}
// bardzo prosze nie modyfikowac klasy Main

class Main {
    public static void main(String[] args) {
        // prosze zmodyfikowac sciezke aby prawidlowo odczytac
        // plik w wlasnym systemie operacyjnym
        final String CSV_FILE = "ekstraklasa.csv";
        final String SPLIT_BY = ";";

        CsvReport csvReport = new CsvReader(CSV_FILE);
        LineConverter lineConverter = new LineConverterImpl();

        List<String> lines = csvReport.csvToListOfString();

        List<Team> teams = lineConverter.toTeam(lines, SPLIT_BY);

        for(Team team: teams) {
            System.out.printf("%2d: %-25s: Punkty: %2d, Zwyciestwa: %2d, Porazki: %2d, Remisy: %2d \n",
                    team.getId(), team.getName(), team.getPoints(), team.getWins(), team.getLoses(),
                    team.getDraws());
        }
    }
}
